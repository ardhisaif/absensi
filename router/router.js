const express = require("express");
const auth = require("../middleware/auth");
const login = require("../controller/auth/login");
const logout = require("../controller/auth/logout");
const refreshToken = require("../controller/auth/refreshToken");
const fcmToken = require("../controller/auth/fcmToken");
const schedule = require("../controller/schedule/schedule");
const attendanceOverview = require("../controller/attendance/attendanceOverview")
const checkClockinHome = require("../controller/attendance/checkClockinHome")
const router = express.Router();

router.get("/", (req, res) => {
	res.send("masuk");
});

router.post("/api/v1/employee/authentication/login", login);
router.get("/api/v1/employee/authentication/logout", auth, logout);
router.post("/api/v1/employee/authentication/refresh", auth, refreshToken);
router.put("/api/v1/employee/fcm-token", auth, fcmToken);

router.get("/api/v1/employee/schedule", auth, schedule)

router.get("/api/v1/employee/attendance-overview", auth, attendanceOverview)
router.post("/api/v1/employee/check-button-clockin", auth, checkClockinHome)

module.exports = router;
