module.exports = function(number) {
    if (number === null) {
        return "00:00:00"
    }

    let numstring = number.toString() 
    for (let i = numstring.length; i < 6; i++) {
        numstring = "0" + numstring
    }

    let result = ""
    for (let i = 0; i < numstring.length; i++) {
        if (i % 2 === 0 && i !== 0) {
            result += ":"
        }

        result += numstring[i]
    }
    let split = result.split(":")
    let secon = Number(split[2])
    let minute = Number(split[1])
    let hour = Number(split[0])

    while ( secon > 59) {
        secon -= 60
        minute += 1
    }

    while ( minute > 59) {
        minute -= 60
        hour += 1
    }

    let seconString = secon.toString()
    let minuteString = minute.toString()
    let hourString = hour.toString()

    if(seconString.length === 1){
        seconString = "0" + seconString
    }

    if(minuteString.length === 1){
        minuteString = "0" + minuteString
    }

    if(hourString.length === 1){
        hourString = "0" + hourString
    }

    const data = hourString + ":" + minuteString + ":" + seconString

    return data
}