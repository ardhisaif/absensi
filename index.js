require("dotenv").config();
const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const router = require("./router/router")

app.use(bodyParser.json());
app.use(router)
const port = process.env.PORT 

app.listen(port, () => {
	console.log(`Server is running in localhost:${port}`);
});