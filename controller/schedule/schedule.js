const jwt = require("jsonwebtoken");
const knex = require("../../config/connectDB");
const { attachPaginate } = require("knex-paginate");
attachPaginate();

module.exports = async (req, res) => {
	const token = req.headers["authorization"].split(" ")[1];
	const perPage = +req.query.per_page;
	const page = +req.query.page;

	try {
		console.log(page, perPage);
		const user = jwt.verify(token, process.env.JWT_SECRET);
		const schedule = await knex
			.select("*")
			.from("schedules")
			.where("user_id", user.id)
			.paginate({ perPage: perPage, currentPage: page });


		let response = []
		
        for (let i = 0; i < schedule.data.length; i++) {

			const shift = await knex
			.select("*")
			.from("shifts")
			.where("id", schedule.data[i].shift_id)

			const data = {
				id: schedule.data[i].id,
				date: schedule.data[i].date,
				is_leave: schedule.data[i].is_leave,
				shift: {
					name: shift[0].name,
					time_start: shift[0].time_start,
					time_end: shift[0].time_end
				}
			}
			response.push(data)
        }
        
		res.json({ data :response });
	} catch (error) {
		return res.status(400).send({
			meta: {
				success: false,
				error,
			},
		});
	}
};
