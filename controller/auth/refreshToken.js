const jwt = require("jsonwebtoken");

module.exports = async (req, res) => {
	try {
		const authHeader = req.headers["authorization"].split(" ")[1];
		console.log(authHeader);
		const token = jwt.sign({ token: authHeader }, process.env.JWT_SECRET, {
			expiresIn: "20160",
		});
		if (!token) {
			return res.status(403).send("A token is required for authentication");
		}

		const meta = {
			success: true,
			error: "Success Refresh Token",
		};
		res.json({ token, meta });
	} catch (error) {
		return res.status(400).send({
			meta: {
				success: false,
				error,
			},
		});
	}
};
