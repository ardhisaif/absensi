const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const knex = require("../../config/connectDB");

module.exports = async (req, res) => {
	const email = req.body.email;
	const password = req.body.password;

	const data = await knex
		.select("*")
		.from("users")
		.where("email", email)
		.offset(0)
		.limit(1);

	if (data.length === 0) {
		return res.status(400).send({
			meta: {
				success: false,
				error: "Invalid Credentials, Please Check Again",
			},
		});
	}

	const checkPassword = bcrypt.compare(password, data[0].password);

	checkPassword
		.then((result) => {
			if (result === true) {
				const id = data[0].id;
				const name = data[0].name;
				const email = data[0].email;
				const token = jwt.sign({ id, name, email }, process.env.JWT_SECRET, {
					expiresIn: "20160m",
				});

				const response = {
					id,
					name,
					email,
					role: "employee",
				};

				const meta = {
					success: true,
					message: "Success Login",
					pagination: [],
				};

				return res.json({ data: response, token: token, meta });
			} else {
				return res.status(400).send({
					meta: {
						success: false,
						error: "Invalid Credentials, Please Check Again",
					},
				});
			}
		})
		.catch(() => {
			return res.status(400).send({
				meta: {
					success: false,
					error: "Invalid Credentials, Please Check Again",
				},
			});
		});
};
