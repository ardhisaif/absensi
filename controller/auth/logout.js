const jwt = require("jsonwebtoken");

module.exports = async (req, res) => {
	try {
		const authHeader = req.headers["authorization"];
	jwt.sign(authHeader, "", { expiresIn: 1 }, (logout, err) => {
		if (logout) {
            const meta = {
                success: true,
                error: "Success Logout",
            }
			res.json({ meta });
		} else {
			res.send({ msg: err });
		}
	});
	} catch (error) {
		return res.status(400).send({
			meta: {
				success: false,
				error,
			},
		});
	}
};
