const jwt = require("jsonwebtoken");
const knex = require("../../config/connectDB");

module.exports = async (req, res) => {
	try {
		const token = req.headers["authorization"].split(" ")[1];
		const { fcm_token } = req.body;

		const decoded = jwt.verify(token, process.env.JWT_SECRET);
		const updateFcmToken = await knex
			.select("*")
			.from("users")
			.where("email", decoded.email)
			.update({ fcm_token });

		if (updateFcmToken === 1) {
			const data = {
				fcm_token,
			};
			const meta = {
				success: true,
				message: "Success Update FCM Token",
				pagination: {},
			};

			res.json({ data, meta });
		} else {
			const meta = {
				success: false,
				message: "Failed Update FCM Token",
				pagination: {},
			};
			res.status(400).send({ meta });
		}
	} catch (error) {
		return res.status(400).send({
			meta: {
				success: false,
				error: "Invalid Credentials, Please Check Again",
			},
		});
	}
};
