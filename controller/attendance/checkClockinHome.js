const jwt = require("jsonwebtoken");
const knex = require("../../config/connectDB");

module.exports = async (req, res) => {
	try {
		const token = req.headers["authorization"].split(" ")[1];
		const today = new Date().toISOString().slice(0, 10)
		const yesterday = new Date((new Date()).valueOf() - 1000*60*60*24).toISOString().split("T")
		const user = jwt.verify(token, process.env.JWT_SECRET);
		const maxDate = await knex("attendances")
			.where("user_id", user.id)
			.max("date_clock as maxDate");

		const lastDate = maxDate[0].maxDate.toISOString().slice(0, 10);
		
		if (lastDate === yesterday[0]) {
			const attendance = await knex("attendances")
				.where("user_id", user.id)
				.where("date_clock", today);

			let is_already_clockout = false;
			let type = "clockin";
			
			if (attendance[0].clock_out !== null) {
				is_already_clockout = true;
				type = "clockout";
			}

			const data = {
				type,
				message_type: null,
				is_already_clockout,
				break_type: null,
			};

			const meta = {
				success: true,
				message: "Success Check Button",
			};

			return res.json({ data, meta });
		}

		const data = {
			type: "clockin",
			message_type: null,
			is_already_clockout: false,
			break_type: null,
		};

		const meta = {
			success: true,
			message: "Success Check Button",
		};

		return res.json({ data, meta });
	} catch (error) {
		console.log(error);
		return res.status(400).send({
			meta: {
				success: false,
				error: "error",
			},
		});
	}
};
