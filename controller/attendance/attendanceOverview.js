const knex = require("../../config/connectDB");
const jwt = require("jsonwebtoken");
const time = require("../../helper/timeFormatter");

module.exports = async (req, res) => {
	try {
		const token = req.headers["authorization"].split(" ")[1];
		const date = req.query.date;
		const arrDate = date.split("-");
		const firstDate = `${arrDate[0]}-${arrDate[1]}-0`;
		const user = jwt.verify(token, process.env.JWT_SECRET);
		const attendance = knex("attendances")
			.where("user_id", user.id)
			.where(function () {
				this.where("date_clock", date).orWhere("date_clock", "<", date);
			})
			.andWhere("updated_at", ">", firstDate);

		const sum = await attendance.sum({
			total_work_hours: "total_work_hours",
			total_late_clock_in: "total_late_clock_in",
			total_early_clock_out: "total_early_clock_out",
		});

		const presentCount = await attendance
			.whereNotNull("clock_in")
			.count({ count: "*" });
		const ontimeCount = await attendance
			.where("is_late_clock_in", 0)
			.andWhere("is_early_clock_out", 0)
			.count({ count: "*" });
		const clockoutEarlyCount = await attendance
			.where("is_early_clock_out", 1)
			.count({ count: "*" });
		const lateCount = await attendance
			.where("is_late_clock_in", 1)
			.count({ count: "*" });
		const absentCount = await attendance
			.where("clock_in", null)
			.count({ count: "*" });
		const employee = await knex("employees").where("user_id", user.id);
		const employee_id = employee[0].id;
		const leave = knex("leaves")
			.where("employee_id", employee_id)
			.where("start_date", ">", firstDate);

		const leaveId = await leave;
		let leaves = [];
		for (let i = 0; i < leaveId.length; i++) {
			const name = await knex("leave_types").where(
				"id",
				leaveId[i].leave_type_id,
			);
			
			const data = {
				id: leaveId[i].id,
				name: name[0].name,
				total: 1,
			};

			leaves.push(data);
		}
		const leavesCount = await leave.count({ count: "*" });
		const total_work_hours = time(sum[0].total_work_hours);
		const total_late_hours = time(sum[0].total_late_clock_in);
		const total_early_hours = time(sum[0].total_early_clock_out);
		const present = presentCount[0].count;
		const ontime = ontimeCount[0].count;
		const clockout_early = clockoutEarlyCount[0].count;
		const late = lateCount[0].count;
		const absent = absentCount[0].count;
		const total_leaves = leavesCount[0].count;
		const total_remaining_leaves = 5 - total_leaves;

		const data = {
			present,
			ontime,
			clockout_early,
			late,
			absent,
			holiday: 0,
			total_work_hours,
			total_late_hours,
			total_early_hours,
			total_leaves,
			total_remaining_leaves,
			leaves,
		};
		const meta = {
			success: true,
			message: "Success Get Attendance Overview",
			pagination: {},
		};

		res.json({ data, meta });
	} catch (error) {
		return res.status(400).send({
			meta: {
				success: false,
				error,
			},
		});
	}
};
